from datetime import datetime

from django.db import models


class CsDbTypes(models.Model):
    name = models.CharField(max_length=200, unique=True)

    class Meta:
        db_table = "cs_db_types"


class CsExternalDb(models.Model):
    name = models.CharField(max_length=200, unique=True)
    db_type = models.ForeignKey("CsDbTypes", models.DO_NOTHING, db_column="db_type")
    db_ip = models.CharField(max_length=200)
    db_port = models.PositiveIntegerField()
    db_driver = models.CharField(max_length=200)
    db_engine = models.CharField(max_length=200)
    db_dbase = models.CharField(max_length=200)
    db_user = models.CharField(max_length=200)
    db_passwd = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "cs_external_db"


class CsExternalApi(models.Model):
    name = models.CharField(max_length=200, unique=True)
    api_type = models.ForeignKey("CsDbTypes", models.DO_NOTHING, db_column="db_type")
    api_server = models.CharField(max_length=200)
    api_port = models.PositiveIntegerField()
    api_user = models.CharField(max_length=200)
    api_passwd = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "cs_external_api"
